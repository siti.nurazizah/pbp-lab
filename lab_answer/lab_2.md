1. Perbedaan antara JSON dan XML?
Answer : Meskipun XML dan JSON format paling umum untuk pertukaran data dan keduanya dapat digunakan untuk menerima data dari web server. Kedua hal ini memiliki perbedaan,yakni:

- JSON yang merupakan singkatan dari JavaScript object notation hanyalah suatu format data(bahasa meta),sedangakan XML yang merupakaan singkatan dari extensive markup languange  merupakan suatu bahasa markup.
- JSON sendiri merupakan format berbasis text untuk pertukaran data, sedangkan XML adalah format independen perangkat lunak untuk pertukaran data
- Secara kompleksitas, JSON lebih sederhana dan mudah dibaca, sedangkan XML lebih rumit.
- Berdasarkan orientasinya, JSON berorientasi pada data, sedangkan XML berorientasi pada dokumen.
- Untuk Array,JSON mendukung array, sedangkan XML tidak mendukung array.
- Untuk ekstensi file, file JSON diakhiri dengan ekstensi
.json, sedangkan file XML diakhiri dengan ekstensi .xml

2. Perbedaan antara HTML dan XML?
Answer :  Baik HTML maupun XML merupakan contoh dari bahasa markup. Keduanya saling melengkapi walaupun terdapat perbedaan di antara  keduanya. Beberapa contoh perbedaan tersebut adalah:

- Hypertext Markup Language (HTML) lebih memiliki fokus pada penyajian data, sedangkan Extensible Markup Language (XML) lebih memiliki fokus pada transfer data 
- HTML merupakan bahasa yang tidak peka terhadap Case Sensitive atau dengan kata lain,HTML tidak peka pada huruf besar dan kecil tiap huruf dan kalimatnya, sedangkan XML merupakan bahasa yang peka atau memperhatikan Case Sensitivenya.
- HTML tidak terlalu ketat pada tag penutup, sedangkan XML sangat ketat dalam arti XML sangat memperhatikan tag penutup.
- HTML memiliki tag yang sedikit terbatas, sedangkan XML memiliki tag yang biasanya dapat dikembangkan kembali
- HTML memiliki tag yang sudah ditentukan sebelumnya, seperti <table>,<hl>,<p>, dll. Sedangkan, biasanya tag XML tidak ditentukan sebelumnya.

