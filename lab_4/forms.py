# import form class from django
from django.utils.translation import gettext_lazy as _
from django import forms
from django.forms import widgets
from lab_2.models import Note

# https://stackoverflow.com/questions/3367091/whats-the-cleanest-simplest-to-get-running-datepicker-in-django



class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields=['To','From','Title','Message']

        To = forms.CharField(max_length='50', required=True)
        From = forms.CharField(max_length='50', required=True)
        Title = forms.CharField(max_length='100', required=True)
        Message = forms.Textarea()

        labels = {
            'To': _('Message Receiver (To)'),
            'From': _('Message Sender (From)'),
            'Title': _('Message Title'),
            'Message': _('Content'),
        }

        widgets = {
            'Message': forms.Textarea(),
        }

        error_messages = {
		    'required' : 'Input cannot be empty',
	    }

