from django.contrib import admin
# Register your models here.
from django.contrib.admin.sites import AdminSite
from .models import Note
admin.site.register(Note)
